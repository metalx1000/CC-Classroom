extends KinematicBody
#based on tutorial by GarbajYT
#https://www.youtube.com/watch?v=Nn2mi5sI8bM
#original code license MIT - https://github.com/GarbajYT/godot_updated_fps_controller/blob/main/LICENSE
#Modified code by Kris Occhipinti 2021-08
#Modified code licensed under GPLv3

export var speed = 7
var current_speed = speed
export var run_speed = 20
var running = 0
const ACCEL_DEFAULT = 7
const ACCEL_AIR = 1
onready var accel = ACCEL_DEFAULT
export var gravity = 50

var jump = 15

var cam_accel = 40
var mouse_sense = 0.1
var snap

var direction = Vector3()
var velocity = Vector3()
var gravity_vec = Vector3()
var movement = Vector3()

onready var head = $head
onready var camera = $head/Camera
onready var step_detector_bottom = $StepDetect/Bottom
onready var step_detector_top = $StepDetect/Top
onready var ray = $head/Camera/RayCast

func _ready():
	#hides the cursor
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
	$StepDetect.rotate_y(1)
	get_tree().call_group("baddies", "set_camera", camera)
	camera(delta)
	walk(delta)
	jump(delta)
	step_detection(delta)
	move(delta)
	touch()
	
func touch():
	if !ray.is_colliding():
		return
	
	var touched = ray.get_collider()

	#if touched.is_in_group("action_objects"):
	if Input.is_action_just_pressed("activate"):
		if !touched.get_owner().active:
			touched.get_owner().active = true

func _input(event):
	#get mouse input for camera rotation
	if event is InputEventMouseMotion:
		rotate_y(deg2rad(-event.relative.x * mouse_sense))
		head.rotate_x(deg2rad(-event.relative.y * mouse_sense))
		head.rotation.x = clamp(head.rotation.x, deg2rad(-89), deg2rad(89))

func camera(delta):
	#camera physics interpolation to reduce physics jitter on high refresh-rate monitors
	if Engine.get_frames_per_second() > Engine.iterations_per_second:
		camera.set_as_toplevel(true)
		camera.global_transform.origin = camera.global_transform.origin.linear_interpolate(head.global_transform.origin, cam_accel * delta)
		camera.rotation.y = rotation.y
		camera.rotation.x = head.rotation.x
	else:
		camera.set_as_toplevel(false)
		camera.global_transform = head.global_transform
		
func walk(delta):
	#get keyboard input
	direction = Vector3.ZERO
	var h_rot = global_transform.basis.get_euler().y
	var f_input = Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	var h_input = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	direction = Vector3(h_input, 0, f_input).rotated(Vector3.UP, h_rot).normalized()
	running = Input.get_action_strength("run") - 1
	current_speed = speed + abs(run_speed * running)
	

func jump(delta):
	#jumping and gravity
	if is_on_floor():
		snap = -get_floor_normal()
		accel = ACCEL_DEFAULT
		gravity_vec = Vector3.ZERO
	else:
		snap = Vector3.DOWN
		accel = ACCEL_AIR
		gravity_vec += Vector3.DOWN * gravity * delta
		
	if Input.is_action_just_pressed("jump") and is_on_floor():
		snap = Vector3.ZERO
		gravity_vec = Vector3.UP * jump
	
	

func move(delta):
	#make it move
	velocity = velocity.linear_interpolate(direction * current_speed, accel * delta)
	movement = velocity + gravity_vec
	
	move_and_slide_with_snap(movement, snap, Vector3.UP)

func step_detection(delta):
	if step_detector_bottom.is_colliding() && !step_detector_top.is_colliding():
		#snap = Vector3.ZERO
		snap = Vector3.ZERO
		gravity_vec = Vector3.UP * 5
		move(delta)
		
		
