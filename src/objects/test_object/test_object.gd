extends Spatial


var active = false

func _process(delta):
	if active:
		rotate_x(.1)
		rotate_y(.1)
		$Timer.wait_time = 2


func _on_Timer_timeout():
	active = false
